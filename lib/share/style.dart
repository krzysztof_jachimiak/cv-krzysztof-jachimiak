import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final TextStyle styleC = TextStyle(color: Colors.purple, fontSize: 30);

final TextStyle styleprose = GoogleFonts.roboto(
    color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16);
final TextStyle styleheading = GoogleFonts.lato(
    color: Colors.grey[900], fontWeight: FontWeight.w700, fontSize: 18);
final BoxDecoration dekoracja = BoxDecoration(
  borderRadius: BorderRadius.circular(8),
);
